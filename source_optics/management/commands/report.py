# Copyright 2019 SourceOptics Project Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from django.core.management.base import BaseCommand, CommandError
from source_optics.models import Organization, Repository, Author, FileChange

# usage:
# python3 manage.py debug --organization=exampleOrg --mode=filechange --what=foo@example.com

class Command(BaseCommand):
    help = 'runs a series of number of different text output reports'

    # FIXME: all of this implementation is a rough cut, including the fuzzy matching to decide what address should be the primary.
    # it's all open to change.

    def add_arguments(self, parser):
        # parser.add_argument('-o', '--organization', dest='org', type=str, help='scan repositories from this organization', default=None)
        parser.add_argument('-m', '--mode', dest='mode', type=str, help='selects the report mode', default=None)
        parser.add_argument('-w', '--what', dest='what', type=str, help='what object to report on', default=None)

    def handle(self, *args, **kwargs):

        org = kwargs.get('org', None)
        mode = kwargs.get('mode', None)
        what = kwargs.get('what', None)

        mode_name = f"report_{mode}"
        handler = getattr(self, mode_name, None)

        if handler is not None:
            handler(what)
        else:
            raise CommandError("mode (-m) is required")

    def report_organization(self, what):
        # FIXME: here, -w should be the same as -o

        organizations = Organization.objects

        if what:
            organizations = Organization.objects.filter(name=what)

        for o in organizations.all():
            print(o)

    def report_repository(self, what):
        # FIXME: this ignores -o

        repos = Repository.objects

        if what:
            repos = repos.filter(name=what)

        for r in repos.all():
            fmt = f"{r} - {r.organization} - {r.url} - {r.enabled}"
            print(fmt)

    def report_filechange(self, what):
        # FIXME: this ignores -o

        file_changes = FileChange.objects

        if what:

            author = Author.objects.get(email=what)
            file_changes = file_changes.filter(
                commit__author = author
            )

        for f in file_changes.all():

            fmt = f"{f.commit.sha} - {f.file.path}/{ f.file.name} -A={ f.lines_added } R={ f.lines_removed } C={f.is_create} M={ f.is_move }"
            print(fmt)

