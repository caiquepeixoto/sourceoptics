#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 18:07:17 2020

@author: jgwilli8
"""

import functools
from django.db import models
from django.db.models import Sum

class Log(models.Model):
    
    TYPES = (
            ('Individual', 'Individual'),
            ('Team', 'Team'),
            ('Lecture', 'Lecture'),
            )
    
    log_id = models.PositiveIntegerField()
    organization = models.ForeignKey('Organization', db_index=True, on_delete=models.CASCADE, blank=False, null=True, related_name='logs')
    author = models.ForeignKey('Author', db_index=True, on_delete=models.CASCADE, blank=False, null=True, related_name='logs')
    date = models.DateTimeField(db_index=True, blank=False, null=True)
    log_type = models.CharField(max_length=20, choices=TYPES)
    hours = models.DecimalField(default=0.0, decimal_places=5, max_digits=12)
    comments = models.TextField()
    
    class Meta:
        verbose_name = 'Log'
        verbose_name_plural = 'Logs'
        index_together = [
                    ('author', 'date'),
                ]

    @classmethod
    def queryset_for_range(cls, orgs, authors=None, start=None, end=None):
        """
        Filters log objects by either organization(s) or author(s) and by 
        the given date interval
        """
        assert orgs or authors

        objs = cls.objects

        if authors:
            objs = objs.filter(author__pk__in=authors)
        if orgs:
            objs = objs.filter(organization__pk__in=orgs)
        if start:
            assert end is not None
            objs = objs.filter(date__range=(start,end))
        return objs

    @classmethod
    def aggregate_stats(cls, org, author=None, start=None, end=None):
        """
        Filters log objects using queryset_for_range and then aggregates 
        log hours to be rolled up into a statistic
        """
        authors = None
        orgs = None
        if author:
            authors = [ author.pk ]
        if org:
            orgs = [ org.pk ]

        qs = cls.queryset_for_range(orgs=orgs, authors=authors, start=start, end=end)
        stats = qs.aggregate(
            total_log_hours = Sum("hours")
        )
        return stats

    @classmethod
    @functools.lru_cache(maxsize=128, typed=False)
    def log_count(cls, org, author=None, start=None, end=None):
        """
        Returns the number of logs within the given interval and for the given
        organization or organization and author
        """
        if author:
            qs = cls.queryset_for_range(orgs=[org.pk], authors=[author.pk], start=start, end=end)
        else:
            qs = cls.queryset_for_range(orgs=[org.pk], start=start, end=end)
        return qs.count()

    @classmethod
    def cache_clear(cls):
        cls.log_count.cache_clear()

    def __str__(self):
        #Untested
        return f"{self.date:%Y-%m-%d} ({self.logtype}, {self.hours} hours) - {self.comments}"