# Generated by Django 2.2.1 on 2020-01-17 15:43

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import source_optics.models.repository


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(db_index=True, max_length=512, null=True, unique=True)),
                ('display_name', models.CharField(blank=True, db_index=True, max_length=512, null=True)),
                ('alias_for', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='alias_of', to='source_optics.Author')),
            ],
        ),
        migrations.CreateModel(
            name='Commit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sha', models.CharField(db_index=True, max_length=512)),
                ('commit_date', models.DateTimeField(db_index=True, null=True)),
                ('author_date', models.DateTimeField(null=True)),
                ('subject', models.TextField(db_index=True)),
                ('author', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='commits', to='source_optics.Author')),
            ],
        ),
        migrations.CreateModel(
            name='Credential',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=255)),
                ('username', models.CharField(blank=True, help_text='for github/gitlab username', max_length=64)),
                ('password', models.TextField(blank=True, help_text='for github/gitlab imports', null=True)),
                ('ssh_private_key', models.TextField(blank=True, help_text='for cloning private repos', null=True)),
                ('ssh_unlock_passphrase', models.CharField(blank=True, help_text='for cloning private repos', max_length=255, null=True)),
                ('description', models.TextField(blank=True, max_length=1024, null=True)),
                ('organization_identifier', models.CharField(blank=True, help_text='for github/gitlab imports', max_length=256, null=True)),
                ('import_filter', models.CharField(blank=True, help_text='if set, only import repos matching this fnmatch pattern', max_length=255, null=True)),
                ('api_endpoint', models.CharField(blank=True, help_text='for github/gitlab imports off private instances', max_length=1024, null=True)),
            ],
            options={
                'verbose_name': 'Credential',
                'verbose_name_plural': 'Credentials',
            },
        ),
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField(db_index=True, max_length=256, null=True)),
                ('path', models.TextField(db_index=True, max_length=256, null=True)),
                ('ext', models.TextField(max_length=32, null=True)),
                ('binary', models.BooleanField(default=False)),
                ('deleted', models.BooleanField(db_index=True, default=False)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='files', to='source_optics.Commit')),
            ],
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=255, unique=True)),
                ('webhook_enabled', models.BooleanField(default=False)),
                ('webhook_token', models.CharField(blank=True, max_length=255, null=True)),
                ('checkout_path_override', models.CharField(blank=True, help_text='if set, override the default checkout location', max_length=512, null=True)),
                ('scanner_directory_allow_list', models.TextField(blank=True, help_text='if set, fnmatch patterns of directories to require, one per line', null=True)),
                ('scanner_directory_deny_list', models.TextField(blank=True, help_text='fnmatch patterns or prefixes of directories to exclude, one per line', null=True)),
                ('scanner_extension_allow_list', models.TextField(blank=True, help_text='if set, fnmatch patterns of extensions to require, one per line', null=True)),
                ('scanner_extension_deny_list', models.TextField(blank=True, help_text='fnmatch patterns or prefixes of extensions to exclude, one per line ', null=True)),
                ('include_merge_commits', models.BooleanField(blank=True, default=False)),
                ('admins', models.ManyToManyField(help_text='currently unused', related_name='_organization_admins_+', to=settings.AUTH_USER_MODEL)),
                ('credential', models.ForeignKey(help_text='used for repo imports and git checkouts', null=True, on_delete=django.db.models.deletion.SET_NULL, to='source_optics.Credential')),
                ('members', models.ManyToManyField(help_text='currently unused', related_name='_organization_members_+', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Repository',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=64, validators=[source_optics.models.repository.validate_repo_name])),
                ('enabled', models.BooleanField(default=True, help_text='if false, disable scanning')),
                ('last_scanned', models.DateTimeField(blank=True, null=True)),
                ('last_pulled', models.DateTimeField(blank=True, null=True)),
                ('url', models.CharField(db_index=True, help_text='use a git ssh url for private repos, else http/s are ok', max_length=255)),
                ('force_next_pull', models.BooleanField(default=False, help_text='used by webhooks to signal the scanner')),
                ('webhook_token', models.CharField(blank=True, help_text='prevents against trivial webhook spam', max_length=255, null=True)),
                ('force_nuclear_rescan', models.BooleanField(default=False, help_text='on next scan loop, delete all commits/records and rescan everything')),
                ('scanner_directory_allow_list', models.TextField(blank=True, help_text='if set, fnmatch patterns of directories to require, one per line', null=True)),
                ('scanner_directory_deny_list', models.TextField(blank=True, help_text='fnmatch patterns or prefixes of directories to exclude, one per line', null=True)),
                ('scanner_extension_allow_list', models.TextField(blank=True, help_text='if set, fnmatch patterns of extensions to require, one per line', null=True)),
                ('scanner_extension_deny_list', models.TextField(blank=True, help_text='fnmatch patterns or prefixes of extensions to exclude, one per line ', null=True)),
                ('organization', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='repos', to='source_optics.Organization')),
            ],
            options={
                'verbose_name_plural': 'repositories',
            },
        ),
        migrations.CreateModel(
            name='Statistic',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateTimeField(null=True)),
                ('interval', models.TextField(choices=[('DY', 'Day'), ('WK', 'Week'), ('MN', 'Month'), ('LF', 'Lifetime')], max_length=5)),
                ('lines_added', models.IntegerField(blank=True, null=True)),
                ('lines_removed', models.IntegerField(blank=True, null=True)),
                ('lines_changed', models.IntegerField(blank=True, null=True)),
                ('commit_total', models.IntegerField(blank=True, null=True)),
                ('files_changed', models.IntegerField(blank=True, null=True)),
                ('author_total', models.IntegerField(blank=True, null=True)),
                ('days_active', models.IntegerField(blank=True, default=0)),
                ('average_commit_size', models.IntegerField(blank=True, default=0, null=True)),
                ('commits_per_day', models.FloatField(blank=True, default=0, null=True)),
                ('files_changed_per_day', models.FloatField(blank=True, default=0, null=True)),
                ('lines_changed_per_day', models.FloatField(blank=True, default=0, null=True)),
                ('bias', models.IntegerField(blank=True, default=0, null=True)),
                ('flux', models.FloatField(blank=True, default=0, null=True)),
                ('commitment', models.FloatField(blank=True, default=0, null=True)),
                ('earliest_commit_date', models.DateTimeField(blank=True, null=True)),
                ('latest_commit_date', models.DateTimeField(blank=True, null=True)),
                ('days_since_seen', models.IntegerField(default=-1, null=True)),
                ('days_before_joined', models.IntegerField(default=-1, null=True)),
                ('longevity', models.IntegerField(blank=True, default=0)),
                ('last_scanned', models.DateTimeField(blank=True, null=True)),
                ('moves', models.IntegerField(blank=True, default=0)),
                ('edits', models.IntegerField(blank=True, default=0)),
                ('creates', models.IntegerField(blank=True, default=0)),
                ('author', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='author', to='source_optics.Author')),
                ('repo', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='repo', to='source_optics.Repository')),
            ],
        ),
        migrations.CreateModel(
            name='FileChange',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lines_added', models.IntegerField(default=0)),
                ('lines_removed', models.IntegerField(default=0)),
                ('is_create', models.IntegerField(default=0)),
                ('is_move', models.IntegerField(default=0)),
                ('is_edit', models.IntegerField(default=0)),
                ('commit', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='file_changes', to='source_optics.Commit')),
                ('file', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='file_changes', to='source_optics.File')),
            ],
        ),
        migrations.AddField(
            model_name='file',
            name='repo',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='source_optics.Repository'),
        ),
        migrations.AddField(
            model_name='commit',
            name='repo',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='commits', to='source_optics.Repository'),
        ),
        migrations.AddIndex(
            model_name='statistic',
            index=models.Index(fields=['start_date', 'interval', 'repo', 'author'], name='author_rollup3'),
        ),
        migrations.AlterUniqueTogether(
            name='statistic',
            unique_together={('start_date', 'interval', 'repo', 'author')},
        ),
        migrations.AddIndex(
            model_name='repository',
            index=models.Index(fields=['name', 'organization'], name='source_opti_name_5846e6_idx'),
        ),
        migrations.AlterUniqueTogether(
            name='repository',
            unique_together={('name', 'organization')},
        ),
        migrations.AddIndex(
            model_name='filechange',
            index=models.Index(fields=['file', 'commit'], name='file_change2'),
        ),
        migrations.AlterUniqueTogether(
            name='filechange',
            unique_together={('file', 'commit')},
        ),
        migrations.AddIndex(
            model_name='file',
            index=models.Index(fields=['repo', 'name', 'path'], name='file2'),
        ),
        migrations.AddIndex(
            model_name='file',
            index=models.Index(fields=['repo', 'path'], name='file3'),
        ),
        migrations.AddIndex(
            model_name='file',
            index=models.Index(fields=['repo', 'name', 'path', 'deleted'], name='file4'),
        ),
        migrations.AlterUniqueTogether(
            name='file',
            unique_together={('repo', 'name', 'path')},
        ),
        migrations.AddIndex(
            model_name='commit',
            index=models.Index(fields=['commit_date', 'author', 'repo'], name='commit3'),
        ),
        migrations.AddIndex(
            model_name='commit',
            index=models.Index(fields=['author_date', 'author', 'repo'], name='commit4'),
        ),
        migrations.AddIndex(
            model_name='commit',
            index=models.Index(fields=['author', 'repo'], name='commit5'),
        ),
        migrations.AlterUniqueTogether(
            name='commit',
            unique_together={('repo', 'sha')},
        ),
    ]
